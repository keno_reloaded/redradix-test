## Run dev
`npm run dev`

## Then open in browser
`http://localhost:3300/`

## Run build
`npm run prod`

## Stack utilizado

*  Bootstrap
*  SASS-Lang
*  PUGJs
*  Webpack