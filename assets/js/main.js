$(function() {
  $("#viewMoreBtn").on("click", function() {
    var btnText = $("#viewMoreBtnText").text();
    if(btnText === "View More") {
      $("#viewMoreBtnText").text("View Less")
    } else {
      $("#viewMoreBtnText").text("View More")
    }
    $("#viewMoreDesc").toggleClass("show-less show-more");
    $("#viewMoreBtn i").toggleClass("arrow__up arrow__down");
  });
  $('[data-toggle="tooltip"]').tooltip();
});
