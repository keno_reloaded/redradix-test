import "../assets/scss/main.scss";

import "../node_modules/jquery/src/jquery.js";

import $ from "jquery";
window.jQuery = $;
window.$ = $;

import "../node_modules/bootstrap/dist/js/bootstrap.js";

//Main site javascript
import "../assets/js/main.js";
