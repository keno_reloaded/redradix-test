// webpack v4
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const WebpackMd5Hash = require('webpack-md5-hash');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");


module.exports = {
  entry: { main: './views/index.js' },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[contenthash].js'
  },
  devtool:'source-map',
  devServer: {
    contentBase: './dist',
    port: 3300
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin(),
      new OptimizeCSSAssetsPlugin()
    ]
  },
  module: {
    rules: [
      {
        test: /.pug$/,
        use: [
          {
            loader: 'pug-loader'
          }
        ]
      },
      {
        test: /\.m?js$/,
          exclude: /(node_modules)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
      },
      {
        test: /\.(scss|css)$/,
      //   use: [
      //       'style-loader', // creates style nodes from JS strings
      //       MiniCssExtractPlugin.loader,
      //       'css-loader', // translates CSS into CommonJS
      //       'sass-loader' // compiles Sass to CSS, using Node Sass by default
      //   ]
      use: [
          {
            loader: 'style-loader'
          },
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }]
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true,
              disable: true
            }
          }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|otf|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'fonts/'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: './assets/img', to: 'img' },
      { from: './favicon.png', to: 'favicon.png' }
    ]),

    new MiniCssExtractPlugin({
      filename: '[contenthash].css'
    }),
    new HtmlWebpackPlugin({
      template: './views/home/index.pug',
      filename: 'index.html',
      page: 'home'
    }),
    new CleanWebpackPlugin(['dist']),
    new WebpackMd5Hash()
  ]
};
